package org.easy.excel.test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.ss.usermodel.Workbook;
import org.easy.excel.ExcelContext;
import org.easy.excel.result.ExcelImportResult;
import org.easy.excel.util.ExcelUtil;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

/**
 * ExcelUtil 测试类
 * @author lisuo
 *
 */
public class ExcelUtiltTest {
	
	
	private static ExcelContext context = new ExcelContext("excel-config.xml");
	
	public static void main(String[] args) throws Exception {
		testGetErrorWorkbook();
	}
	
	public static void testGetErrorWorkbook() throws Exception {
		Resource resource = new ClassPathResource("test-excel-error.xlsx");
		ExcelImportResult result = context.readExcel("student2", 2, resource.getInputStream(),true);
		if(result.hasErrors()){
			Workbook errorWorkbook = ExcelUtil.getErrorWorkbook(result.getErrors(), resource.getInputStream(), 0, 2,true);
			errorWorkbook.write(new FileOutputStream("src/test/resources/test-excel-error-result.xlsx"));
		}
	}
	
	public static void testReadExcel() throws IOException {
		System.err.println("读取src/test/resources/test-excel.xlsx文件");
		ClassPathResource resource = new ClassPathResource("test-excel.xlsx");
		List<List<Object>> list = ExcelUtil.readExcel(resource.getInputStream(), 0);
		for (int i = 0; i < list.size(); i++) {
			// 获取每一行
			System.out.println("第" + (i + 1) + " 行的数据如下");
			List<Object> rows = list.get(i);
			// 便利每一行中的元素
			for (int j = 0; j < rows.size(); j++) {
				Object cellValue = rows.get(j);
				System.out.print("第【" + (i + 1) + "】行第【" + (j + 1) + "】列的值是" + "【" + cellValue + "】");
				System.out.print("    ");
			}
			System.out.println();
		}
		resource.getInputStream().close();
	}
	
}
