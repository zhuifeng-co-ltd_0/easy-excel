package org.easy.excel.aop.cell.inner;

import org.easy.excel.aop.cell.CellInterceptorAdapter;
import org.easy.excel.aop.cell.CellJoinPoint;
import org.easy.excel.exception.ExcelException;

/**
 * 处理导入excel,如果是字符类型的数据,去除前后空格
 * @author lisuo
 *
 */
public class TrimStringCellInterceptor extends CellInterceptorAdapter {

	@Override
	public Object executeImport(CellJoinPoint joinPoint) throws ExcelException {
		if (joinPoint.getValue() instanceof String) {
			return joinPoint.getValue().toString().trim();
		}
		return super.executeImport(joinPoint);
	}

}
